api = 2
core = 8.x

; Modules

projects[entity_reference_revisions][version] = "1.x-dev"
projects[entity_reference_revisions][type] = "module"
projects[entity_reference_revisions][subdir] = "contrib"
projects[entity_reference_revisions][download][type] = "git"
projects[entity_reference_revisions][download][branch] = "8.x-1.x"

projects[paragraphs][version] = "1.x-dev"
projects[paragraphs][type] = "module"
projects[paragraphs][subdir] = "contrib"
projects[paragraphs][download][type] = "git"
projects[paragraphs][download][branch] = "8.x-1.x"
